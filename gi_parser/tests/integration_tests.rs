#![cfg(test)]

use speculoos::prelude::*;

use gi_parser::{build_lookup_table, parse};
use gi_parser::parser::VerbClause::{IntransitiveVerb, TransitiveVerb};
use gi_parser::parser::VerbPhrase;
use gi_parser::tokens::{DirectObject, Verb};

#[test]
fn stage_should_return_just_stage() {
    let result = parse(build_lookup_table(), "stage foo");
    assert_that(&result).is_ok().is_equal_to(&VerbPhrase {
        meta: None,
        clause: Some(IntransitiveVerb(Verb::Stage)),
        args: "foo".to_string(),
    });
}

#[test]
fn list_changes_should_return_verb_and_do() {
    let result = parse(build_lookup_table(), "list changes");
    assert_that(&result).is_ok().is_equal_to(&VerbPhrase {
        meta: None,
        clause: Some(TransitiveVerb {
            verb: Verb::List,
            direct_object: Some(DirectObject::Change),
        }),
        args: "".to_string(),
    });
}

#[test]
fn help_list_should_return_meta_help_and_verb_list() {
    let result = parse(build_lookup_table(), "help list");
    assert_that(&result).is_ok().is_equal_to(&VerbPhrase {
        meta: Some(Verb::Help),
        clause: Some(TransitiveVerb {
            verb: Verb::List,
            direct_object: None,
        }),
        args: "".to_string(),
    });
}

#[test]
fn complete_list_changes_should_return_meta_list_and_args() {
    let result = parse(build_lookup_table(), "complete list ch");
    assert_that(&result).is_ok().is_equal_to(&VerbPhrase {
        meta: Some(Verb::Complete),
        clause: Some(TransitiveVerb {
            verb: Verb::List,
            direct_object: Some(DirectObject::Change),
        }),
        args: "".to_string(),
    });
}

#[test]
fn complete_with_multiple_args_should_return_all_args() {
    let result = parse(build_lookup_table(), "complete list ch lib.rs ev");
    assert_that(&result).is_ok().is_equal_to(&VerbPhrase {
        meta: Some(Verb::Complete),
        clause: Some(TransitiveVerb {
            verb: Verb::List,
            direct_object: Some(DirectObject::Change),
        }),
        args: "lib.rs ev".to_string(),
    });
}

#[test]
fn transitive_verb_with_unknown_do_returns_as_arg() {
    let result = parse(build_lookup_table(), "list x");

    assert_that(&result).is_ok().is_equal_to(&VerbPhrase {
        meta: None,
        clause: Some(TransitiveVerb {
            verb: Verb::List,
            direct_object: None,
        }),
        args: "x".to_string(),
    })
}
