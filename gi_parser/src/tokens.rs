use std::collections::HashMap;
use std::rc::Rc;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Verb {
    Help,
    List,
    Clone,
    Create,
    Nuke,
    Unnuke,
    Remove,
    Stage,
    Unstage,
    Switch,
    Complete,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum DirectObject {
    Branch,
    Change,
    Commit,
    Remote,
    Tag,
}

pub type TokenMap<T> = HashMap<String, T>;

pub struct TokenLookup {
    pub meta_verbs: Rc<TokenMap<Verb>>,
    pub intransitive_verbs: Rc<TokenMap<Verb>>,
    pub transitive_verbs: Rc<TokenMap<Verb>>,
    pub direct_objects: Rc<TokenMap<DirectObject>>,
}
