#![allow(dead_code)]

use std::rc::Rc;

use nom::Err::{Error, Failure, Incomplete};

pub use gi_parser_error::GiParserError;
use sugars::hmap;
use tokens::TokenMap;

use crate::DirectObject::*;
use crate::parser::verb_phrase;
use crate::parser::VerbPhrase;
use crate::tokens::{DirectObject, TokenLookup, Verb};
use crate::Verb::*;

mod fuzzy;
mod gi_parser_error;
mod nom_utils;
pub mod parser;
pub mod tokens;
pub mod util;



pub fn parse(
    token_lookup: Rc<TokenLookup>,
    input: &str,
) -> Result<VerbPhrase, GiParserError<&str>> {
    let (input, mut phrase) = verb_phrase(token_lookup)(input).map_err(|err| match err {
        Error(e) | Failure(e) => e,
        Incomplete(_) => GiParserError::GenericError,
    })?;

    phrase.args = input.to_string();
    Ok(phrase)
}

pub fn build_lookup_table() -> Rc<TokenLookup> {
    Rc::new(TokenLookup {
        meta_verbs: Rc::new(meta_verbs()),
        intransitive_verbs: Rc::new(intransitive_verbs()),
        transitive_verbs: Rc::new(transitive_verbs()),
        direct_objects: Rc::new(direct_objects()),
    })
}

pub fn meta_verbs() -> TokenMap<Verb> {
    hmap! {
        "help".to_owned() => Help,
        "complete".to_owned() => Complete
    }
}

pub fn intransitive_verbs() -> TokenMap<Verb> {
    hmap! {
        "clone".to_owned() => Clone,
        "stage".to_owned() => Stage,
        "unstage".to_owned() => Unstage,
        "switch".to_owned() => Switch,
    }
}

pub fn transitive_verbs() -> TokenMap<Verb> {
    hmap! {
        "nuke".to_owned() => Nuke,
        "unnuke".to_owned() => Unnuke,
        "list".to_owned() => List,
        "create".to_owned() => Create,
        "remove".to_owned() => Remove,
    }
}

pub fn direct_objects() -> TokenMap<DirectObject> {
    hmap! {
        "branch".to_owned() => Branch,
        "change".to_owned() => Change,
        "commit".to_owned() => Commit,
        "remote".to_owned() => Remote,
        "tag".to_owned() => Tag,
    }
}
