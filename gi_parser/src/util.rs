use nom::bytes::complete::{take_till, take_while};
use nom::character::is_space;
use nom::combinator::peek;
use nom::IResult;

pub fn take_word() -> impl Fn(&str) -> IResult<&str, &str> {
    move |input: &str| {
        let (remainder, word) = take_till(char_is_whitespace)(input)?;
        let (remainder, _) = eat_whitespace(remainder)?;
        Ok((remainder, word))
    }
}

pub fn peek_word<'a>() -> impl Fn(&'a str) -> IResult<&str, &str> {
    move |input: &str| peek(take_till(char_is_whitespace))(input)
}

pub fn eat_whitespace(input: &str) -> IResult<&str, &str> {
    take_while(char_is_whitespace)(input)
}

pub fn char_is_whitespace(ch: char) -> bool {
    is_space(ch as u8)
}

#[cfg(test)]
mod tests {
    use speculoos::assert_that;
    use speculoos::prelude::*;

    use crate::util::{peek_word, take_word};

    #[test]
    fn take_word_should_return_word() {
        let phrase: String = "hello world".to_owned();
        let result = take_word()(phrase.as_str());
        assert_that(&result).is_ok().is_equal_to(("world", "hello"));
    }

    #[test]
    fn multiple_take_word_is_valid() {
        let phrase: String = "hello world".to_owned();
        let (input, hello) = take_word()(phrase.as_str()).unwrap();
        let (_, world) = take_word()(input).unwrap();
        assert_that(&hello).is_equal_to("hello");
        assert_that(&world).is_equal_to("world");
    }

    #[test]
    fn peek_word_returns_word_does_not_consume() {
        let phrase: String = "hello world".to_owned();
        let result = peek_word()(phrase.as_str());
        assert_that(&result)
            .is_ok()
            .is_equal_to(("hello world", "hello"));
    }
}
