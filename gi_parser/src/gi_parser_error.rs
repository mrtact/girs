extern crate nom;

use nom::error::ErrorKind;
use nom::error::ParseError;
use nom::IResult;

#[derive(Debug, PartialEq)]
pub enum GiParserError<I> {
    GenericError,
    NoFuzzyMatch,
    AmbiguousMatch(Vec<String>),
    Nom(I, ErrorKind),
}

impl<I> ParseError<I> for GiParserError<I> {
    fn from_error_kind(input: I, kind: ErrorKind) -> Self {
        GiParserError::Nom(input, kind)
    }

    fn append(_: I, _: ErrorKind, other: Self) -> Self {
        other
    }
}

impl<I> From<nom::error::Error<I>> for GiParserError<I> {
    fn from(err: nom::error::Error<I>) -> Self {
        GiParserError::Nom(err.input, err.code)
    }
}

fn parse(_input: &str) -> IResult<&str, &str, GiParserError<&str>> {
    Err(nom::Err::Error(GiParserError::GenericError))
}

#[cfg(test)]
mod tests {
    use super::parse;
    use super::GiParserError;
    use nom::Err::Error;

    #[test]
    fn it_works() {
        let err = parse("").unwrap_err();
        match err {
            Error(e) => assert_eq!(e, GiParserError::GenericError),
            _ => panic!("Unexpected error: {:?}", err),
        }
    }
}
