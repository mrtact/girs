use crate::fuzzy::fuzzy_match;
use crate::tokens::TokenLookup;
use crate::util::{peek_word, take_word};
use crate::{DirectObject, GiParserError, Verb};
use log::debug;
use nom::branch::alt;
use nom::combinator::opt;
use nom::{Err, IResult};
use std::rc::Rc;

#[derive(Debug, PartialEq)]
pub struct VerbPhrase {
    // Options like help & complete are meta verbs
    pub meta: Option<Verb>,
    pub clause: Option<VerbClause>,
    pub args: String,
}

impl Default for VerbPhrase {
    fn default() -> VerbPhrase {
        VerbPhrase {
            meta: None,
            clause: None,
            args: "".to_string(),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum VerbClause {
    IntransitiveVerb(Verb),
    TransitiveVerb {
        verb: Verb,
        direct_object: Option<DirectObject>,
    },
}

pub fn verb_phrase<'a>(
    map: Rc<TokenLookup>,
) -> impl Fn(&str) -> IResult<&str, VerbPhrase, GiParserError<&str>> + 'a {
    move |input: &str| {
        let (input, meta) = opt(meta_verb(map.clone()))(input).map_err(Err::convert)?;

        let (input, clause) =
            alt((intransitive_verb(map.clone()), transitive_verb(map.clone())))(input)
                .map_err(Err::convert)?;

        let phrase = VerbPhrase {
            meta,
            clause: Some(clause),
            args: input.to_string(),
        };

        debug!("{:?}", phrase);

        Ok((input, phrase))
    }
}

fn meta_verb<'a>(
    map: Rc<TokenLookup>,
) -> impl Fn(&str) -> IResult<&str, Verb, GiParserError<&str>> + 'a {
    move |input: &str| {
        let (input, token) = peek_word()(input).map_err(Err::convert)?;

        let verb = fuzzy_match(map.meta_verbs.clone(), token).map_err(nom::Err::Error)?;

        // If we found a valid match, eat the token + whitespace
        let (input, _) = take_word()(input).map_err(Err::convert)?;
        Ok((input, verb))
    }
}

fn intransitive_verb<'a>(
    map: Rc<TokenLookup>,
) -> impl Fn(&str) -> IResult<&str, VerbClause, GiParserError<&str>> + 'a {
    move |input: &str| {
        let (input, token) = peek_word()(input).map_err(Err::convert)?;

        let verb = fuzzy_match(map.intransitive_verbs.clone(), token).map_err(nom::Err::Error)?;

        let (input, _) = take_word()(input).map_err(Err::convert)?;
        Ok((input, VerbClause::IntransitiveVerb(verb)))
    }
}

fn transitive_verb(
    map: Rc<TokenLookup>,
) -> impl Fn(&str) -> IResult<&str, VerbClause, GiParserError<&str>> {
    move |input: &str| {
        let (input, token) = peek_word()(input).map_err(Err::convert)?;

        let verb = fuzzy_match(map.transitive_verbs.clone(), token).map_err(nom::Err::Error)?;

        let (input, _) = take_word()(input).map_err(Err::convert)?;

        let (input, direct_object) = direct_object(map.clone())(input).map_err(Err::convert)?;

        Ok((
            input,
            VerbClause::TransitiveVerb {
                verb,
                direct_object,
            },
        ))
    }
}

fn direct_object(
    map: Rc<TokenLookup>,
) -> impl Fn(&str) -> IResult<&str, Option<DirectObject>, GiParserError<&str>> {
    move |input: &str| {
        let (input, token) = peek_word()(input).map_err(Err::convert)?;

        // Direct object is optional, because we might be doing a complete or help
        // call. If we don't get a match, then set the DO to None. If we do get a
        // match, eat the token and set Some(DO). For any other error, hoist the error.
        let result = fuzzy_match(map.direct_objects.clone(), token);

        let direct_object = match result {
            Ok(n) => Ok(Some(n)),
            Err(err) => match err {
                GiParserError::NoFuzzyMatch => Ok(None),
                _ => Err(nom::Err::Error(err)),
            },
        }?;

        let input = if direct_object.is_some() {
            let (input, _) = take_word()(input).map_err(Err::convert)?;
            input
        } else {
            input
        };

        Ok((input, direct_object))
    }
}
