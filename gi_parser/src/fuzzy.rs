use log::debug;
use rust_fuzzy_search::fuzzy_search_sorted;
use std::rc::Rc;

use crate::GiParserError::{AmbiguousMatch, NoFuzzyMatch};
use crate::{GiParserError, TokenMap};

pub fn best_match_candidate<'a>(
    potential: &'a str,
    candidates: Vec<&String>,
) -> Result<(String, u8), GiParserError<&'a str>> {
    let ref_list = candidates
        .iter()
        .map(|o| (*o).as_str())
        .collect::<Vec<&str>>();
    let search = fuzzy_search_sorted(potential, &ref_list);

    debug!("{:?}", search);

    let top_score = if search.is_empty() {
        0f32
    } else {
        search.first().unwrap().1
    };

    let search = search
        .into_iter()
        .filter(|(_, score)| score >= &0.25f32 && (top_score - score).abs() < f32::EPSILON)
        .collect::<Vec<(&'_ str, f32)>>();

    match search.len() {
        0 => Err(NoFuzzyMatch),
        1 => {
            let m = search.first().unwrap();
            Ok((m.0.to_owned(), (m.1 * 100f32) as u8))
        }
        _ => Err(AmbiguousMatch(
            search.into_iter().map(|(key, _)| key.to_owned()).collect(),
        )),
    }
}

pub fn fuzzy_match_string<'a, T>(
    token_map: &'a TokenMap<T>,
    token: &'a str,
) -> Result<String, GiParserError<&'a str>> {
    let keys = token_map.keys().collect();
    let (val, _) = best_match_candidate(token, keys)?;
    Ok(val)
}

pub fn fuzzy_match<T: Copy>(
    token_map: Rc<TokenMap<T>>,
    token: &str,
) -> Result<T, GiParserError<&str>> {
    let keys = token_map.keys().collect();
    let (result, _) = best_match_candidate(token, keys)?;
    token_map
        .get(result.as_str())
        .map(|o| o.to_owned())
        .ok_or(GiParserError::NoFuzzyMatch)
}

#[cfg(test)]
mod tests {
    use speculoos::prelude::*;

    use crate::fuzzy::fuzzy_match_string;
    use crate::GiParserError;
    use crate::{intransitive_verbs, transitive_verbs};

    #[test]
    fn stage_should_match<'a>() {
        let map = intransitive_verbs();
        vec!["st", "stg", "stg", "stag", "stage"]
            .iter()
            .for_each(|o| {
                assert_that(&fuzzy_match_string(&map, &o.to_string()))
                    .is_ok()
                    .is_equal_to(String::from("stage"));
            });
    }

    #[test]
    fn list_should_match<'a>() {
        let map = transitive_verbs();
        vec!["ls", "lis", "list"].iter().for_each(|o| {
            assert_that(&fuzzy_match_string(&map, &o.to_string()))
                .is_ok()
                .is_equal_to(String::from("list"));
        });
    }

    #[test]
    fn asdf_should_not_match<'a>() {
        let map = transitive_verbs();
        assert_that(&fuzzy_match_string(&map, &"asdf".to_string()))
            .is_err()
            .matches(|err| {
                match err {
                    GiParserError::NoFuzzyMatch => true, // We're good
                    _ => false,
                }
            });
    }

    /*
        #[test]
        fn ambiguous_search_should_return_error<'a>() {
            assert_that(&fuzzy_match(&"un)(to_string()))
                .is_err()
                .matches(|err| err.is::<FuzzyError>())
                .matches(|err| {
                    match err.downcast_ref::<FuzzyError>().unwrap() {
                        FuzzyError::AmbiguityError(_) => true,
                        _ => false
                    }
                });

            // TODO: Unpack the error, make sure it has the right members
        }
    */
}
