use nom::error::{ErrorKind, ParseError};

pub fn nom_error<I, E>(input: I, kind: ErrorKind) -> E
where
    I: Sized,
    E: ParseError<I>,
{
    E::from_error_kind(input, kind)
}
