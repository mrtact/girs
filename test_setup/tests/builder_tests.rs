#![allow(non_snake_case)]

use path_macro::path;
use speculoos::prelude::*;
use test_setup::RepoBuilder;

#[test]
fn pop_branch__should_remove_branch_file() {
    let setup = RepoBuilder::new()
        .with_branch("test-branch")
        .with_test_file("readme.md", "# README")
        .pop_branch()
        .build();


    assert_that(&setup.repo.get_current_branch().unwrap().name())
        .matches(|o| o.as_str() == "main");

    let file_path = path!(setup.repo.root() / "readme.md");
    assert_that(&file_path).does_not_exist();
}