use std::fs::File;
use std::io::BufWriter;
use std::io::Write;

use git2::{Repository, RepositoryInitOptions, Signature};
use path_macro::path;
use temp_dir::TempDir;

use git_wrapper::repo::Repo;

pub mod builders;

pub struct TestRepo<'repo> {
    pub repo: Repo<'repo>,
    pub cloned_repo: Option<Repo<'repo>>,

    // We own these so the temp dirs get deleted when the struct gets dropped
    pub repo_path: TempDir,
    cloned_path: Option<TempDir>,
}

impl<'repo> TestRepo<'repo> {
    /**
    Borrowed from git2-rs
    Creates a test repo in a temp directory, then sets some basic defaults
    to make it functional.
     */
    fn new() -> TestRepo<'repo> {
        let td = TempDir::new().unwrap();
        let repo_path = path!(td.path() / "test");
        let mut opts = RepositoryInitOptions::new();
        opts.initial_head("main");
        let repo = Repository::init_opts(&repo_path, &opts).unwrap();
        {
            let mut config = repo.config().unwrap();
            config.set_str("user.name", "name").unwrap();
            config.set_str("user.email", "email").unwrap();
            let mut index = repo.index().unwrap();
            let id = index.write_tree().unwrap();

            let tree = repo.find_tree(id).unwrap();
            let sig = repo.signature().unwrap();
            repo.commit(Some("HEAD"), &sig, &sig, "initial\n\nbody", &tree, &[])
                .unwrap();
        }

        TestRepo {
            repo: Repo::at(repo_path.as_path()).unwrap(),
            cloned_repo: None,
            repo_path: td,
            cloned_path: None,
        }
    }

    pub fn commit_test_file(&self, file_name: &str, content: &str) {
        let file_path = path!(self.repo.root() / file_name);
        let f = File::create(file_path.as_path()).unwrap();
        let mut writer = BufWriter::new(f);
        write!(writer, "{}", content).unwrap();
        let _ = writer.flush();

        self.repo.add(file_path);
        let author = Signature::now("Testy McTesterson", "testy@acme.com").unwrap();
        self.repo.commit(&author, "Test commit".into());
    }
}
