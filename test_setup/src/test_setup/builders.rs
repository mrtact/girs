use temp_dir::TempDir;

use git_wrapper::repo::Repo;

use crate::TestRepo;

pub struct RepoBuilder<'repo> {
    setup: TestRepo<'repo>,
}

impl<'repo> RepoBuilder<'repo> {
    pub fn new() -> RepoBuilder<'repo> {
        RepoBuilder {
            setup: TestRepo::new()
        }
    }

    pub fn with_clone(mut self) -> Self {
        let td = TempDir::new().unwrap();

        Repo::git_clone(
            self.setup.repo.root().to_str().unwrap(),
            &td.path(),
        ).unwrap();

        let repo = Repo::at(&td.path()).unwrap();
        self.setup.cloned_repo = Some(repo);
        self.setup.cloned_path = Some(td);
        self
    }

    pub fn with_test_file(
        self,
        file_name: &str,
        content: &str,
    ) -> Self {
        self.setup.commit_test_file(file_name, content);
        self
    }

    pub fn with_branch(self, branch_name: &'repo str) -> BranchBuilder {
        let parent_name: String;
        {
            let parent = &self.setup.repo.get_current_branch().unwrap();
            self.setup.repo.create_branch(
                &parent,
                branch_name
            ).unwrap();
            parent_name = parent.name();
        }
        let _ = self.setup.repo.checkout_branch(branch_name);
        BranchBuilder {
            prev_branch_name: parent_name,
            repo_builder: self
        }
    }

    pub fn build(self) -> TestRepo<'repo> {
        self.setup
    }
}

pub struct BranchBuilder<'a> {
    prev_branch_name: String,
    repo_builder: RepoBuilder<'a>,
}

impl<'a> BranchBuilder<'a> {
    pub fn with_test_file(
        self,
        branch_file_name: &str,
        content: &str
    ) -> Self {
        self.repo_builder.setup.commit_test_file(
            branch_file_name, content
        );

        self
    }

    pub fn pop_branch(self) -> RepoBuilder<'a> {
        self.repo_builder.setup.repo.checkout_branch(&self.prev_branch_name).unwrap();
        self.repo_builder
    }
}
