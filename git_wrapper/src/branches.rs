use crate::branch::Branch;
use git2::{Branches as Git2Branches};

pub struct Branches<'repo>(Git2Branches<'repo>);

impl<'repo> Iterator for Branches<'repo> {
    type Item = Branch<'repo>;

    fn next(&mut self) -> Option<Self::Item> {
        self.0
            .next()?
            .map(|(git2branch, _)| git2branch.into())
            .ok()
    }
}

impl<'repo> From<Git2Branches<'repo>> for Branches<'repo> {
    fn from(wrapped: Git2Branches<'repo>) -> Self {
        Branches(wrapped)
    }
}
