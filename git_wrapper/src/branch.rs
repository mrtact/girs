use git2::BranchType::{Local, Remote};
use git2::{Branch as Git2Branch, BranchType, Commit, Error, Oid, Reference as Git2Reference};

pub struct Branch<'repo>(Git2Branch<'repo>);

impl <'repo> Branch<'repo> {
    pub fn name(&self) -> String {
        self.0
            .name()
            .unwrap_or_default()
            .unwrap_or_default()
            .to_owned()
    }

    pub fn upstream(&self) -> Option<Branch<'_>> {
        self.0.upstream().map(Branch::from).ok()
    }

    pub fn set_upstream(&mut self, upstream_name: &str) -> Result<(), Error> {
        self.0.set_upstream(Some(upstream_name))
    }

    pub fn oid(&self) -> Oid {
        self.0.get().target().expect(
            "Could not unwrap the underlying reference when trying to extract a branch's OID",
        )
    }

    pub fn branch_type(&self) -> BranchType {
        if self.0.get().is_remote() {
            Remote
        } else {
            Local
        }
    }

    pub fn into_reference(self) -> Git2Reference<'repo> {
        self.0.into_reference()
    }
}

impl<'repo> From<Git2Branch<'repo>> for Branch<'repo> {
    fn from(wrapped: Git2Branch<'repo>) -> Self {
        Branch::<'repo>(wrapped)
    }
}

impl<'repo> From<Git2Reference<'repo>> for Branch<'repo> {
    fn from(ref_: Git2Reference<'repo>) -> Self {
        let git2br = Git2Branch::wrap(ref_);
        git2br.into()
    }
}

impl<'repo> From<Branch<'repo>> for Commit<'repo> {
    fn from(branch: Branch<'repo>) -> Self {
        let ref_: Git2Reference<'repo> = branch.0.into_reference();

        ref_.peel_to_commit()
            .expect("Could not convert git branch into a commit")
    }
}
