pub mod cache_dir;
pub mod repo_url;

use git2::{Cred, RemoteCallbacks, Repository};
use home::home_dir;
use path_macro::path;
use std::path::Path;

pub fn get_credentials() -> RemoteCallbacks<'static> {
    // TODO Need a general purpose credentials handler here

    // let git_config = Config::open_default().unwrap();

    let mut cb = RemoteCallbacks::new();
    cb.credentials(|_url, username_from_url, _allowed_types| {
        let private_key = path!(home_dir().unwrap() / ".ssh" / "id_ed25519");
        let public_key = private_key.clone().with_extension("pub");

        Cred::ssh_key(
            username_from_url.unwrap(),
            Some(&public_key.as_path()),
            &private_key,
            None,
        )
    });

    // let mut cb = RemoteCallbacks::new();
    // cb.credentials(move |url, username, allowed| {
    //     Cred::credential_helper(
    //         &git_config,
    //         &url,
    //         username
    //     )
    // });

    cb
}

pub fn is_repo(path: &Path) -> bool {
    Repository::open(path).is_ok()
}
