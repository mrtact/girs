use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::marker::PhantomData;
use std::path::{Path, PathBuf};

use git2::Repository;
use path_macro::path;

use crate::Branches;
use crate::error::GitWrapperError::NotAGitRepo;

pub mod ahead_behind;
mod branch;
mod clone;
mod worktree;
mod add;

pub struct Repo<'repo> {
    pub repo: Repository,
    pub symlink: PathBuf,
    phantom: PhantomData<&'repo Repository>,
}

impl<'repo> Repo<'_> {
    pub fn new() -> anyhow::Result<Repo<'repo>> {
        let path = PathBuf::from(env::var("PWD").unwrap());
        Repo::at(path.as_path())
    }

    pub fn at(path: &Path) -> anyhow::Result<Repo<'repo>> {
        Ok(Repo {
            repo: Repository::open(&path)
                    .map_err(|_| NotAGitRepo((&path.display()).to_string()))?,
            symlink: path.to_path_buf(),
            phantom: Default::default(),
        })
    }

    pub fn branches(&self) -> Branches {
        Branches::from(
            self.repo
                    .branches(None)
                    .expect("Could not get branches from repo"),
        )
    }

    pub fn default_branch_name(&self) -> anyhow::Result<String> {
        let mut head_path = path!(self.repo.path() / "refs" / "remotes" / "origin" / "HEAD" );
        if !head_path.exists() {
            head_path = path!(self.repo.path() / ".git" / "HEAD" );
        }

        read_branch_name_from_head_file(head_path)
    }

    pub fn root(&self) -> PathBuf {
        let mut path = PathBuf::from(self.repo.path());
        path.pop();
        path
    }
}

fn read_branch_name_from_head_file(file_path: PathBuf) -> anyhow::Result<String> {
    let head = File::open(file_path)?;
    let buf_reader = BufReader::new(head);
    let line = buf_reader.lines().take(1).next().unwrap()?;
    let name = line.rsplit("/").take(1).next().unwrap().to_owned();

    return Ok(name);
}


impl<'repo> From<Repository> for Repo<'repo> {
    fn from(git_repo: Repository) -> Self {
        let path: PathBuf;
        {
            path = (&git_repo).path().to_path_buf();
        }
        Repo {
            repo: git_repo,
            symlink: path,
            phantom: Default::default(),
        }
    }
}

#[cfg(test)]
mod tests {
    // use test_setup::TestSetup;
    //
    // #[test]
    // fn default_branch_name_should_work() {
    //     let setup = TestSetup::new();
    //     assert!(setup.repo().default_branch_name() == "main");
    // }
}
