use std::fmt;
use std::fmt::{Display, Formatter};

use git2::{Status as G2S, StatusEntry as Git2StatusEntry};
use serde::{Serialize, Serializer};

use crate::status::types::ChangelistState::*;
use crate::status::types::FileState::*;

use super::{Status, StatusEntry};

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum FileState {
    New,
    Modified,
    Deleted,
    Renamed,
    Conflicted,
    Unsupported,
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum ChangelistState {
    Staged,
    Unstaged,
    Conflicted,
    None,
}

// Convert from a single git2 status to a Status struct,
// ensuring that only one status bit is set.
impl From<G2S> for Status {
    fn from(status: G2S) -> Self {
        assert_eq!(status.bits().count_ones(), 1);

        match status {
            G2S::INDEX_NEW => Status::new(Staged, New),
            G2S::INDEX_MODIFIED => Status::new(Staged, Modified),
            G2S::INDEX_DELETED => Status::new(Staged, Deleted),
            G2S::INDEX_RENAMED => Status::new(Staged, Renamed),
            G2S::WT_NEW => Status::new(Unstaged, New),
            G2S::WT_MODIFIED => Status::new(Unstaged, Modified),
            G2S::WT_DELETED => Status::new(Unstaged, Deleted),
            G2S::WT_RENAMED => Status::new(Unstaged, Renamed),
            G2S::CONFLICTED => Status::new(ChangelistState::Conflicted, FileState::Conflicted),
            _ => Status::UNSUPPORTED,
        }
    }
}

/**
Any given status entry may have more than one status flag set (e.g.
the same file can have some staged changes and some unstaged). This
takes an entry and returns a vector of status data, one for each matched flag, each entry having the correct status bit set
 */
pub fn multiplex_status_entries(entry: Git2StatusEntry) -> Vec<StatusEntry> {
    (0u32..=u32::BITS-1)
        .filter_map(|bit| {
            let flag = 1u32 << bit;
            if entry.status().bits() & flag > 0 {
                let status = Status::from(G2S::from_bits_truncate(flag));
                if status == Status::UNSUPPORTED {
                    Option::None
                } else {
                    let path = entry
                        .path()
                        .expect("Got a status entry with an invalid path")
                        .to_owned();
                    Some(StatusEntry { path, status })
                }
            } else {
                Option::None
            }
        })
        .collect()
}

impl Serialize for ChangelistState {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

impl Display for ChangelistState {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Staged => f.write_str("Staged"),
            Unstaged => f.write_str("Unstaged"),
            ChangelistState::Conflicted => f.write_str("Conflicted"),
            None => unimplemented!(),
        }
    }
}

/*
mod tests {
    use git2::{Status as G2S, StatusEntry as G2SE, StatusEntry};
    use crate::status::types::multiplex_status_entries;

    fn multiplex_git_status_yields_correct_enums() {
        // todo!("How to test this, when we can't create a StatusEntry?");

        G2SE::from_raw()
        let git_status = G2S::INDEX_MODIFIED
            .union(G2S::WT_MODIFIED);

        assert!(multiplex_status_entries(git_status) == vec![StatusEntry::])
    }
}
*/
