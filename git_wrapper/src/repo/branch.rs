use std::fs::File;
use time::{Duration, OffsetDateTime};

use anyhow::{anyhow, Context};
use git2::build::CheckoutBuilder;
use git2::Commit;

use AheadBehind::{Ahead, Behind, NotTracking, UpToDate, AB};

use crate::{AheadBehind, Branch};

use super::Repo;

impl<'repo> Repo<'repo> {
    pub fn get_branch(&self, name: &str) -> Option<Branch> {
        self.branches()
            .find(|branch| branch.name().ends_with(name))
    }

    pub fn checkout_branch(&self, branch: &str) -> anyhow::Result<()> {
        let reference = self
            .repo
            .resolve_reference_from_short_name(branch.as_ref())
            .with_context(|| format!("Could not find a branch named {}", branch))?;

        if !reference.is_branch() {
            return Err(anyhow!("{} is not a branch", branch));
        }

        let mut opts = CheckoutBuilder::new();
        opts.force();

        let ref_name = reference.name().unwrap();

        self.repo.set_head(ref_name)?;
        self.repo.checkout_head(Some(&mut opts))?;

        Ok(())
    }

    pub fn create_branch(
        &'repo self,
        parent: &Branch<'repo>,
        name: &str,
    ) -> anyhow::Result<Branch<'repo>> {
        let parent: Branch = self
            .repo
            .find_branch(parent.name().as_str(), parent.branch_type())?
            .into();

        let commit = Commit::from(parent);

        self.repo
            .branch(name, &commit, false)
            .map(Branch::from)
            .map_err(anyhow::Error::from)
    }

    pub fn get_current_branch(&self) -> anyhow::Result<Branch> {
        Ok(Branch::from(self.repo.head()?))
    }

    pub fn get_branch_ahead_behind(&self, branch: &Branch) -> anyhow::Result<AheadBehind> {
        let upstream = match branch.upstream() {
            Some(upstream) => upstream,
            None => return Ok(NotTracking),
        };

        let ab = self.repo.graph_ahead_behind(branch.oid(), upstream.oid());

        ab.map(|o| match o {
            (0, 0) => UpToDate,
            (ahead, 0) => Ahead(ahead),
            (0, behind) => Behind(behind),
            (ahead, behind) => AB(ahead, behind),
        })
            .map_err(|e| anyhow!("Could not get upstream info: {}", e))
    }

    pub fn get_pulled_date(&self) -> anyhow::Result<Duration> {
        let mut fetch_head_path = self.repo.workdir().unwrap().to_path_buf();
        fetch_head_path.push(".git/FETCH_HEAD");

        let duration = if let Ok(fetch_head) = File::open(fetch_head_path) {
            let created = fetch_head.metadata()?.modified()?;
            OffsetDateTime::now_local()? - created
        } else {
            Duration::ZERO
        };

        Ok(duration)
    }

    pub fn create_tracking_branch(
        &'repo self, remote_branch: &'repo Branch, tracking_branch: &str
    ) -> anyhow::Result<Branch<'repo>> {

        let mut new_branch = self.create_branch(&remote_branch, tracking_branch)?;

        // Actually track the remote branch
        new_branch.set_upstream(&remote_branch.name())?;

        Ok(new_branch)
    }

    pub fn track_remote(
        &'repo self, remote_branch: &'repo Branch, branch_name: &str
    ) -> anyhow::Result<Branch<'repo>> {

        let tracking_branch = self.create_tracking_branch(remote_branch, branch_name)?;
        self.checkout_branch(&tracking_branch.name())?;

        Ok(tracking_branch)
    }
}
