use std::path::Path;

use crate::util::get_credentials;
use anyhow::anyhow;
use log::{debug, error};

use super::Repo;

impl<'repo> Repo<'repo> {
    pub fn git_clone(url: &str, path: &Path) -> anyhow::Result<Box<Repo<'repo>>> {
        let mut fo = git2::FetchOptions::new();
        fo.remote_callbacks(get_credentials());

        let mut builder = git2::build::RepoBuilder::new();
        builder.fetch_options(fo);

        debug!("Cloning into path {}", path.display());

        let repo = builder
            .clone(url, path)
            .map_err(|err| {
                error!("Result: {:?}", err);
                anyhow!(err)
            })?
            .into();

        Ok(Box::new(repo))
    }
}
