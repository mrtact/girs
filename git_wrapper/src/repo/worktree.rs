use std::fs::remove_dir;
use std::path::PathBuf;
use git2::WorktreeAddOptions;
use log::debug;
use path_macro::path;
use crate::Branch;
use crate::repo::Repo;
use crate::util::cache_dir;


impl<'repo> Repo<'repo> {
    pub fn worktree_dir(&self, branch_name: &str) -> anyhow::Result<PathBuf> {
        let remote = self.repo.find_remote("origin")?;
        let remote = remote.url().unwrap().replace("/", "_");
        Ok(path!(cache_dir::get().as_path() / remote / branch_name))
    }

    pub fn worktree_root(&self) -> anyhow::Result<PathBuf> {
        self.worktree_dir(&self.default_branch_name()?)
    }

    pub fn get_worktree(&self, worktree_name: &str) -> Option<PathBuf> {
        self.worktree_dir(worktree_name)
            .map_or(None, |o| Some(o))
    }

    pub fn has_worktree(&self, worktree_name: &str) -> bool {
        let dir = self.worktree_dir(worktree_name);
        dir.is_ok() && dir.unwrap().exists()
    }

    pub fn create_worktree(&self, source_branch: Branch, worktree_name: &str) -> anyhow::Result<()>{
        let path = self.worktree_dir(worktree_name)?;
        debug!("Creating worktree for {} at {}", worktree_name, path.display());

        let mut options = WorktreeAddOptions::new();
        let reference = source_branch.into_reference();
        options.reference(Some(&reference));

        match self.repo.worktree(worktree_name, &path, Some(&options)) {
            Ok(w) => debug!("Worktree {} created", w.name().unwrap()),
            Err(e) => {
                debug!("Error creating worktree: {:?}", e)
            }
        }

        Ok(())
    }

    pub fn drop_worktree(&self, worktree: &str) {
        if let Some(path) = self.get_worktree(worktree) {
            remove_dir(path.as_path()).unwrap();
        }
    }
}

