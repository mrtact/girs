use std::path::Path;

use git2::Signature;

use pathdiff::diff_paths;

use crate::repo::Repo;

impl <'repo> Repo<'repo> {
    pub fn add<P: AsRef<Path>>(&self, p: P) {
        let mut index = self.repo.index().unwrap();
        let relative_path = diff_paths(p, self.root()).unwrap();
        index.add_path(relative_path.as_path().as_ref()).unwrap();
        index.write().unwrap();
    }

    pub fn commit(
        &self,
        author: &Signature,
        message: String,
    ) {
        let head = self.repo.head().unwrap();
        let commit = head.peel_to_commit().unwrap();

        let tree_id = self.repo.index().unwrap()
            .write_tree().unwrap();

        let tree = self.repo.find_tree(tree_id).unwrap();

        self.repo.commit(
            Some("HEAD"),
            author,
            author,
            &message,
            &tree,
            &[&commit],
        ).unwrap();
    }
}