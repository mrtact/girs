pub mod types;

use crate::repo::Repo;
use crate::status::types::multiplex_status_entries;
use git2::StatusOptions;

impl Repo<'_> {
    pub fn get_statuses(&self) -> Vec<StatusEntry> {
        let mut options = StatusOptions::new();
        options.include_untracked(true);
        options.recurse_untracked_dirs(true);

        self.repo
            .statuses(Some(&mut options))
            .unwrap()
            .iter()
            .flat_map(multiplex_status_entries)
            .collect()
    }
}

// Describes a single status for a single file: whether it's
// staged or not (or conflicted), and whether it's an add,
// change, remove, etc.  Note that in a list of StatusEntry, you
// can have more than one record for a given file, as a file can
// have both staged & unstaged changes
#[derive(Debug, Eq, PartialEq)]
pub struct Status {
    pub changelist_state: types::ChangelistState,
    pub file_state: types::FileState,
}

impl Status {
    pub const UNSUPPORTED: Status = Status {
        changelist_state: types::ChangelistState::None,
        file_state: types::FileState::Unsupported,
    };

    pub fn new(changelist_state: types::ChangelistState, file_state: types::FileState) -> Self {
        Status {
            changelist_state,
            file_state,
        }
    }

    pub fn tuple(&self) -> (&types::ChangelistState, &types::FileState) {
        (&self.changelist_state, &self.file_state)
    }
}

pub struct StatusEntry {
    pub path: String,
    pub status: Status,
}
