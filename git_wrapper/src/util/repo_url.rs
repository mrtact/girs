use std::convert::TryFrom;

use crate::util::repo_url::RepoUrlType::*;

pub struct RepoUrl {
    pub url: String,
    pub name: String,
    pub typ: RepoUrlType,
}

pub enum RepoUrlType {
    Ssh,
    Http,
    File,
}

impl TryFrom<&str> for RepoUrl {
    type Error = anyhow::Error;

    fn try_from(url: &str) -> anyhow::Result<Self, Self::Error> {
        let repo_url_type = if url.starts_with("git:") {
            Ssh
        } else if url.starts_with("https://") {
            Http
        } else {
            File
        };

        let repo_name = name_from_url(url);

        Ok(RepoUrl {
            url: url.to_owned(),
            name: repo_name,
            typ: repo_url_type,
        })
    }
}

fn name_from_url(url: &str) -> String {
    url.clone()
        .split('/')
        .last().unwrap()
        .trim_end_matches(".git")
        .to_owned()
}

#[cfg(test)]
mod tests {
    use std::path::Component::Normal;

    use temp_dir::TempDir;

    use crate::util::repo_url::name_from_url;

    #[test]
    fn name_from_url_ssh__should_return_name() {
        let ssh_name = name_from_url("git@gitlab.com:mrtact/girs.git");
        assert_eq!(ssh_name, "girs".to_string());
    }

    fn name_from_url_http__should_return_name() {
        let http_name = name_from_url("https://gitlab.com/mrtact/girs.git");
        assert_eq!(http_name, "girs".to_string());
    }

    fn name_from_url_path__should_return_name() {
        let test_dir = TempDir::new().unwrap();
        let last = test_dir.path().components().last().unwrap();

        let last = match last {
            Normal(n) => n.to_str().unwrap().to_owned(),
            _ => panic!()
        };

        let dir_name = name_from_url(&test_dir.path().display().to_string());
        assert_eq!(dir_name, last);
    }
}
