use home::home_dir;
use log::{debug, error, trace};
use path_macro::path;
use std::fs::create_dir_all;
use std::path::PathBuf;

pub fn get() -> PathBuf {
    trace!("Fetching cache dir path");
    let cache_dir = path!(home_dir().unwrap() / ".gi");
    create_if_not_exists(&cache_dir);
    cache_dir
}

pub fn create_if_not_exists(dir: &PathBuf) {
    if !dir.exists() {
        debug!("Directory {} does not exist, creating", dir.display());
        let result = create_dir_all(&dir);
        if let Err(err) = result {
            error!("{:?}", err);
        }
    }
}
