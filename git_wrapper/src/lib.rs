pub use git2::{BranchType, StatusOptions};
pub use BranchType::*;

pub use branch::Branch;
pub use branches::Branches;
pub use repo::ahead_behind::AheadBehind;
pub use status::{Status, StatusEntry};

mod branch;
mod branches;
pub mod repo;
pub mod status;
pub mod util;
mod worktree;
mod error;
