use thiserror::Error;

#[derive(Debug, Error, PartialEq)]
pub enum GitWrapperError {
    #[error("{0} is not a git repo")]
    NotAGitRepo(String),
}
