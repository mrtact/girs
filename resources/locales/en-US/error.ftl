branch-already-exists = The branch '{$name}' already exists
clone-requires-remote = The clone command requires a remote
directory-already-exists = The directory '{$name}' already exists
missing-worktree = There is a local branch '{$name}', but no corresponding worktree
switch-command-requires-branch = The switch command requires a branch

