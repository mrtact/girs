use path_macro::path;
use std::env::current_exe;
use temp_dir::TempDir;

use gi_parser::parser::VerbClause::IntransitiveVerb;
use gi_parser::parser::VerbPhrase;
use gi_parser::tokens::Verb::Clone;
use git_wrapper::repo::Repo;

use girs_lib::global::{Loc, US_ENGLISH};
use girs_lib::executors::clone::CloneCommand;
use girs_lib::executors::git_command::GitCommand;

pub fn setup() {
    let mut path = current_exe().unwrap();
    path.pop();
    if path.ends_with("deps") {
        path.pop();
    }
    let path = path!(path / "resources" / "locales");

    Loc::init(US_ENGLISH, path);
}

pub fn clone_with_worktree(url: &str) -> (Repo, TempDir) {
    let target_dir = TempDir::new().unwrap();
    let cmd = CloneCommand {};
    let mut phrase = VerbPhrase {
        meta: None,
        clause: Some(IntransitiveVerb(Clone)),
        args: format!("{} {}", url, target_dir.path().display()),
    };
    cmd.exec(&mut phrase).unwrap();

    let path = path!(&target_dir.path() / "test");
    (Repo::at(path.as_path()).unwrap(), target_dir)
}
