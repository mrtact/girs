#![allow(non_snake_case)]

use std::env;
use std::env::{current_dir, set_current_dir};
use std::path::PathBuf;
use log::debug;

use path_macro::path;
use speculoos::prelude::*;

use gi_parser::parser::VerbPhrase;
use test_setup::RepoBuilder;

use crate::utils::clone_with_worktree;
use girs_lib::executors::git_command::GitCommand;
use girs_lib::executors::switch::SwitchCommand;

mod utils;

#[test]
fn switch__should_match_branch_name() {
    utils::setup();
    let test_repo = RepoBuilder::new()
        .with_branch("new-branch")
        .pop_branch()
        .build();

    let remote = &test_repo.repo.root().display().to_string();
    let (_, td) = clone_with_worktree(remote);

    let clone_dir = path!( td.path() / "test" );

    set_current_dir(&clone_dir).unwrap();
    env::set_var("PWD", &clone_dir);


    let cwd = PathBuf::from(env::var("PWD").unwrap());
    let current = current_dir().unwrap();

    debug!("Current working dir: {} {}", cwd.display(), current.display());

    let cmd = SwitchCommand;
    let mut args = VerbPhrase {
        meta: None,
        clause: None,
        args: "new-branch".to_string(),
    };
    cmd.exec(&mut args)
        .unwrap_or_else(|err| println!("{}", err));

    assert_that(&test_repo.repo.get_current_branch().unwrap().name())
        .matches(|o| o.as_str() == "new-branch");
}

#[test]
fn switch__should_reveal_branch_file() {
    utils::setup();
    let test_repo = RepoBuilder::new()
        .with_branch("new-branch")
        .with_test_file("branch_file.txt", "Branch file")
        .pop_branch()
        .build();

    let branch_file = path!(&test_repo.repo.root() / "branch_file.txt");
    assert_that(&branch_file).does_not_exist();

    set_current_dir(test_repo.repo.root()).unwrap();
    let cmd = SwitchCommand;
    let mut args = VerbPhrase {
        meta: None,
        clause: None,
        args: "new-branch".to_string(),
    };
    cmd.exec(&mut args)
        .unwrap_or_else(|err| println!("{}", err));

    assert_that(&branch_file).exists();
}