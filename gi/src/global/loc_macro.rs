#[macro_export]
macro_rules! loc {
    ($loc_key:expr) => {
        $crate::global::Loc::translate($loc_key)
    };

    ($loc_key:expr, $loc_data:expr) => {
        $crate::global::Loc::translate_data($loc_key, $loc_data)
    }; // ($($loc_key:expr), ($($literal:literal : $) *)) => {
       //
       // }
}

/// Display a localized string with a single token
#[macro_export]
macro_rules! loc1 {
    ($loc_key:expr, $name:ident) => {
        {
            let data = $crate::hmap!(
                "name".to_owned() => $crate::FluentValue::from($name.clone())
            );
            let trans = $crate::global::Loc::translate_data($loc_key, data).clone();
            trans
        }
    };
}
