use std::collections::HashMap;
use std::fmt;
use std::fmt::{Debug, Formatter};
use std::path::PathBuf;

use fluent_templates::{ArcLoader, Loader};
use fluent_templates::fluent_bundle::FluentValue;
use once_cell::sync::OnceCell;
use unic_langid::{langid, LanguageIdentifier};

pub const US_ENGLISH: LanguageIdentifier = langid!("en-US");
pub const FRANCAIS: LanguageIdentifier = langid!("fr");

pub struct Loc {
    pub loader: ArcLoader,
    pub lang_id: LanguageIdentifier,
}

static INSTANCE: OnceCell<Loc> = OnceCell::new();

impl Loc {
    pub fn init(id: LanguageIdentifier, loc_path: PathBuf) {
        INSTANCE
            .set(Loc {
                loader: ArcLoader::builder(loc_path.as_path(), US_ENGLISH)
                    .customize(|bundle| bundle.set_use_isolating(false))
                    .build()
                    .unwrap(),
                lang_id: id,
            })
            .unwrap()
    }

    pub fn global() -> &'static Loc {
        INSTANCE.get().expect("Loc system is not initialized")
    }

    pub fn translate(key: &str) -> String {
        let Loc { loader, lang_id } = Loc::global();
        loader.lookup(lang_id, key)
    }

    pub fn translate_data(key: &str, data: HashMap<String, FluentValue<'_>>) -> String {
        let Loc { loader, lang_id } = Loc::global();
        loader.lookup_with_args(lang_id, key, &data)
    }
}

impl Debug for Loc {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("Loc")
            .finish()
            .expect("Debugging a Loc failed");

        Ok(())
    }
}
