use anyhow::anyhow;

use gi_parser::parser::{VerbClause, VerbPhrase};
use gi_parser::tokens::DirectObject::*;
use gi_parser::tokens::{DirectObject, Verb};
use list::{ListBranches, ListChanges};
use Verb::*;
use VerbClause::{IntransitiveVerb, TransitiveVerb};

use crate::executors::clone::CloneCommand;
use crate::executors::list;
use crate::executors::list::ListCommand;
use crate::executors::meta::help::HelpCommand;
use crate::executors::switch::SwitchCommand;
use crate::executors::git_command::GitCommand;

pub fn route_command(verb_phrase: &VerbPhrase) -> anyhow::Result<Box<dyn GitCommand>> {
    if let Some(meta) = verb_phrase.meta {
        return match meta {
            Complete => Ok(Box::new(HelpCommand)),
            Help => Ok(Box::new(HelpCommand)),
            _ => Err(anyhow!("Bad meta command")),
        };
    }

    if let Some(clause) = &verb_phrase.clause {
        match clause {
            IntransitiveVerb(verb) => route_verb(verb, &None),
            TransitiveVerb {
                verb,
                direct_object,
            } => route_verb(verb, direct_object),
        }
    } else {
        Err(anyhow!("Bad verb clause"))
    }
}

fn route_verb(
    verb: &Verb,
    direct_object: &Option<DirectObject>,
) -> anyhow::Result<Box<dyn GitCommand>> {
    match verb {
        Clone => Ok(Box::new(CloneCommand)),
        List => route_list(direct_object),
        Switch => Ok(Box::new(SwitchCommand)),
        _ => unimplemented!(),
    }
}

fn route_list(direct_object: &Option<DirectObject>) -> anyhow::Result<Box<dyn GitCommand>> {
    if let Some(direct_object) = direct_object {
        match direct_object {
            Branch => Ok(Box::new(ListBranches)),
            Change => Ok(Box::new(ListChanges)),
            Remote => unimplemented!(),
            Tag => unimplemented!(),
            _ => Err(anyhow!("List doesn't support that direct object")),
        }
    } else {
        Ok(Box::new(ListCommand))
    }
}

pub fn route_switch(_verb_phrase: &VerbPhrase) -> anyhow::Result<Box<dyn GitCommand>> {
    unimplemented!()
}
