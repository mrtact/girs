use std::env::current_exe;
use std::fs;

use fluent_handlebars_runtime::FluentHandlebars;
use handlebars::Handlebars;
use serde_json::Value;

use crate::global::Loc;
use crate::renderer::style_helper::style_helper;

pub mod style;
pub mod styles;

pub mod render_trait;
pub(crate) mod status_display;
mod style_helper;

pub struct Renderer<'a> {
    handlebars: Handlebars<'a>,
}

impl Renderer<'_> {
    pub fn new<'a>() -> Renderer<'a> {
        let mut handlebars = handlebars::Handlebars::new();
        let Loc { loader, .. } = Loc::global();
        handlebars.register_helper("t", Box::from(FluentHandlebars::new(loader)));

        handlebars.register_helper("s", Box::new(style_helper));

        Renderer { handlebars }
    }

    pub fn add_templates(&mut self, template_path: &str) {
        let mut path = current_exe().expect("Couldn't get the path to the current executable");

        path.pop(); // Remove executable name
        path.push("resources/templates");
        path.push(template_path);

        fs::read_dir(&path)
            .expect(format!("Invalid template directory {}", path.display()).as_str())
            .filter_map(|entry| entry.ok())
            .filter(|entry| {
                entry
                    .file_name()
                    .into_string()
                    .expect("Template file name was an invalid string for some reason")
                    .ends_with(".mustache")
            })
            .for_each(|template| {
                let template_name = template
                    .path()
                    .file_stem()
                    .unwrap()
                    .to_str()
                    .unwrap()
                    .to_owned();

                self.handlebars
                    .register_template_file(
                        template_name.as_str(),
                        template.path().to_str().unwrap(),
                    )
                    .unwrap();
            })
    }

    pub fn render(&self, data: &Value) -> anyhow::Result<()> {
        print!("{}", self.handlebars.render("main", &data)?);
        Ok(())
    }
}
