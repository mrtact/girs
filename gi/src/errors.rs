use crate::{loc, loc1};
use std::fmt::{Display, Formatter};

use thiserror::Error;
use GiError::*;

#[derive(Debug, Error, PartialEq)]
pub enum GiError {
    CloneRequiresRemote,
    DirectoryExists(String),
    MissingBranchSpecifier,
    MissingWorktree(String),
}

impl Display for GiError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.display())
    }
}

impl GiError {
    fn display(&self) -> String {
        match &self {
            CloneRequiresRemote => loc!("clone-requires-remote"),
            DirectoryExists(name) => loc1!("directory-already-exists", name),
            MissingBranchSpecifier => loc!("switch-command-requires-branch"),
            MissingWorktree(name) => loc1!("missing-worktree", name),
        }
        .to_string()
    }
}
