use std::env;
use std::path::PathBuf;
use std::time::{Duration, Instant};

use crossterm::event::{poll, read, Event, KeyCode, KeyModifiers};
use git_wrapper::repo::Repo;
use log::debug;

pub mod terminal;

const COUNTDOWN_SECS: u64 = 5;

pub fn count_down<F>(prompt: &str, gerund: &str, f: F) -> anyhow::Result<()>
where
    F: FnOnce() -> anyhow::Result<()>,
{
    {
        // let mut term = TermPrinter::new();
        println!("{}", prompt);

        // If the environment variable IDE is set, don't wait for the countdown
        // This is because crossterm event handling barfs on the poll because the
        // terminal isn't properly initialized
        if env::var("IDE").is_ok() {
            return f();
        }

        print!("{} in ", gerund);

        let mut count: u64 = 0;
        let instant = Instant::now();
        loop {
            let elapsed = instant.elapsed().as_millis();
            if elapsed > (1_000 * COUNTDOWN_SECS) as u128 {
                println!();
                break;
            } else if elapsed >= 1_000 * count as u128 {
                print!("{}... ", COUNTDOWN_SECS - count);
                count += 1;
            }

            if poll(Duration::from_millis(100))? {
                if let Event::Key(event) = read()? {
                    match (event.code, event.modifiers) {
                        (KeyCode::Enter, _) => break,
                        (KeyCode::Char('c'), KeyModifiers::CONTROL) | (KeyCode::Esc, _) => {
                            println!();
                            println!(" *** Cancelled");
                            return Ok(());
                        }
                        _ => assert!(true), // do nothing
                    }
                }
            }
        }
    }

    f()
}

/**
    Walks up the directory tree from the current directory, trying to find
    the symlink to the worktree in the cache directory.
*/
pub fn root_symlink(repo: &Repo) -> Option<PathBuf> {
    let repo_root = repo.root();

    // TODO: Make sure this works consistently on all platforms. Might need
    //  to abstract it with platform-specific functions
    let mut cwd = PathBuf::from(env::var("PWD").unwrap());

    debug!("Current working dir: {}", cwd.display());

    loop {
        if cwd.is_symlink() && cwd.read_link().unwrap() == repo_root {
            return Some(cwd);
        }

        if cwd.parent() == None {
            break;
        }

        cwd.pop();
    }

    None
}
