use crossterm::cursor::{MoveDown, MoveLeft};
use crossterm::execute;
use crossterm::style::Print;
use std::io::stdout;

pub struct TermPrinter {
    pub line_length: u16,
}

impl TermPrinter {
    pub fn new() -> TermPrinter {
        // enable_raw_mode().unwrap();
        TermPrinter { line_length: 0 }
    }

    pub fn print(&mut self, str: &str) {
        self.line_length += str.len() as u16;
        execute!(stdout(), Print(str)).unwrap();
    }

    pub fn println(&mut self, str: &str) {
        self.print(&format!("{}\n", str));
        execute!(stdout(), MoveLeft(self.line_length), MoveDown(1)).unwrap();
        self.line_length = 0;
    }
}

// impl Drop for TermPrinter {
//     fn drop(&mut self) {
//         disable_raw_mode().unwrap();
//     }
// }
