pub mod ahead_behind;
pub mod branch_type;
pub mod changelist_state;
pub mod duration;
pub mod file_state;
mod status;

pub trait Render {
    fn render(&self) -> String;
}
