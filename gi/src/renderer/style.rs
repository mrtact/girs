use crossterm::execute;
use crossterm::style::Attribute::*;
use crossterm::style::Color as CTColor;
use crossterm::style::Color::*;
use crossterm::style::{
    Attribute as CTAttribute, Attributes, ContentStyle, ResetColor, SetAttributes,
    SetBackgroundColor, SetForegroundColor,
};

pub enum Style {
    Attribute(CTAttribute),
    Color(CTColor),
    None,
}

impl From<&str> for Style {
    fn from(style: &str) -> Self {
        if let Some(attr) = attribute_from_style(style) {
            Style::Attribute(attr)
        } else if let Some(color) = color_from_style(style) {
            Style::Color(color)
        } else {
            Style::None
        }
    }
}

fn attribute_from_style(style: &str) -> Option<CTAttribute> {
    match style {
        "bold" => Some(Bold),
        "dim" => Some(Dim),
        "italic" => Some(Italic),
        "underlined" => Some(Underlined),
        "slowblink" => Some(SlowBlink),
        "rapidblink" => Some(RapidBlink),
        "reverse" => Some(Reverse),
        "hidden" => Some(Hidden),
        "crossedout" => Some(CrossedOut),
        "framed" => Some(Framed),
        "encircled" => Some(Encircled),
        "overlined" => Some(OverLined),
        _ => None,
    }
}

fn reset_attribute_from(attribute: &CTAttribute) -> Option<CTAttribute> {
    match attribute {
        Bold => Some(NoBold),
        Dim => Some(NormalIntensity),
        Italic => Some(NoItalic),
        Underlined => Some(NoUnderline),
        SlowBlink => Some(NoBlink),
        RapidBlink => Some(NoBlink),
        Reverse => Some(NoReverse),
        Hidden => Some(NoHidden),
        CrossedOut => Some(NotCrossedOut),
        Framed => Some(NotFramedOrEncircled),
        Encircled => Some(NotFramedOrEncircled),
        OverLined => Some(NotOverLined),
        _ => None,
    }
}

fn color_from_style(style: &str) -> Option<CTColor> {
    match style {
        "black" => Some(Black),
        "darkgrey" => Some(DarkGrey),
        "red" => Some(Red),
        "darkred" => Some(DarkRed),
        "green" => Some(Green),
        "darkgreen" => Some(DarkGreen),
        "yellow" => Some(Yellow),
        "darkyellow" => Some(DarkYellow),
        "blue" => Some(Blue),
        "darkblue" => Some(DarkBlue),
        "magenta" => Some(Magenta),
        "darkmagenta" => Some(DarkMagenta),
        "cyan" => Some(Cyan),
        "darkcyan" => Some(DarkCyan),
        "white" => Some(White),
        "grey" => Some(Grey),
        _ => None,
    }
}

pub trait AnsiStart {
    fn start_token(&self) -> String;
}

pub trait AnsiEnd {
    fn end_token(&self) -> String;
}

impl AnsiStart for ContentStyle {
    fn start_token(&self) -> String {
        let mut buf = Vec::<u8>::new();

        if let Some(bg) = self.background_color {
            execute!(buf, SetBackgroundColor(bg)).unwrap_or(());
        }

        if let Some(fg) = self.foreground_color {
            execute!(buf, SetForegroundColor(fg)).unwrap_or(());
        }

        if !self.attributes.is_empty() {
            execute!(buf, SetAttributes(self.attributes)).unwrap_or(());
        }

        String::from_utf8(buf).unwrap()
    }
}

impl AnsiEnd for ContentStyle {
    fn end_token(&self) -> String {
        let mut buf = Vec::<u8>::new();

        if !self.attributes.is_empty() {
            let mut attributes = Attributes::default();

            CTAttribute::iterator().for_each(|attr| {
                if self.attributes.has(attr) {
                    if let Some(attr) = reset_attribute_from(&attr) {
                        attributes.set(attr);
                    }
                }
            });

            if !attributes.is_empty() {
                execute!(buf, SetAttributes(attributes)).unwrap_or(());
            }
        }

        if self.background_color != None || self.foreground_color != None {
            execute!(buf, ResetColor).unwrap_or(());
        }

        String::from_utf8(buf).unwrap()
    }
}
