use super::Render;
use git_wrapper::{BranchType, Local, Remote};

impl Render for BranchType {
    fn render(&self) -> String {
        match self {
            Local => "Local",
            Remote => "Remote",
        }
        .to_owned()
    }
}
