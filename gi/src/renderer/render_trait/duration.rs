use sugars::hmap;
use time::Duration;

use crate::loc;

use super::Render;

const ONE_MINUTE: i64 = 60;
const ONE_HOUR: i64 = Duration::hours(1).whole_seconds();
const EIGHT_HOURS: i64 = Duration::hours(8).whole_seconds();
const ONE_DAY: i64 = Duration::days(1).whole_seconds();
const THIRTY_DAYS: i64 = Duration::days(30).whole_seconds();

impl Render for Duration {
    fn render(&self) -> String {
        let num: String = "num".to_owned();

        // TODO: This is a workaround until I figure out how to make the loc!() macro parse data
        let (key, data) = match self.whole_seconds() {
            0..=ONE_MINUTE => ("pulled-now", None),
            0..=ONE_HOUR => (
                "pulled-minutes",
                Some(hmap!(
                    num => self.whole_minutes().into()
                )),
            ),
            0..=ONE_DAY => (
                "pulled-hours",
                Some(hmap!(
                    num => self.whole_hours().into()
                )),
            ),
            0..=THIRTY_DAYS => (
                "pulled-days",
                Some(hmap!(
                    num => self.whole_days().into()
                )),
            ),
            _ => ("pulled-long", None),
        };

        let pulled = match data {
            Some(data) => loc!(key, data),
            None => loc!(key),
        };

        if self.whole_seconds() > EIGHT_HOURS {
            format!("❗ Pulled {}", pulled)
        } else {
            format!("Pulled {}", pulled)
        }
    }
}
