use super::Render;
use git_wrapper::status::types::{ChangelistState, FileState};
use git_wrapper::Status;

use ChangelistState::*;
use FileState::*;

use ChangelistState::Conflicted as ChConflicted;
use FileState::Conflicted as FConflicted;

impl Render for Status {
    fn render(&self) -> String {
        match self.tuple() {
            (Staged, New) => "added",
            (Staged, Modified) => "changed",
            (Staged, Deleted) => "removed",
            (Staged, Renamed) | (Unstaged, Renamed) => "renamed",
            (Unstaged, New) => "new",
            (Unstaged, Modified) => "modified",
            (ChConflicted, FConflicted) => "conflicted",
            (_, _) => "",
        }
        .to_owned()
    }
}
