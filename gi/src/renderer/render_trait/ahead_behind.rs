use git_wrapper::AheadBehind;
use AheadBehind::*;

use crate::loc;
use crate::renderer::render_trait::Render;

impl Render for AheadBehind {
    fn render(&self) -> String {
        match &self {
            UpToDate => "--/--".to_owned(),
            Ahead(n) => format!("+{}/--", n),
            Behind(n) => format!("--/-{}", n),
            AB(a, b) => format!("+{}/-{}", a, b),
            NotTracking => loc!("branch-not-tracking"),
        }
    }
}
