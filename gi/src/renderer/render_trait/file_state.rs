use super::Render;
use git_wrapper::status::types::FileState;
use git_wrapper::status::types::FileState::{
    Conflicted, Deleted, Modified, New, Renamed, Unsupported,
};

impl Render for FileState {
    fn render(&self) -> String {
        match self {
            New => "new",
            Modified => "modified",
            Deleted => "deleted",
            Renamed => "renamed",
            Conflicted => "conflicted",
            Unsupported => "unsupported",
        }
        .to_owned()
    }
}
