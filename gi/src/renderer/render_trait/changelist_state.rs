use crate::renderer::render_trait::Render;
use git_wrapper::status::types::ChangelistState;
use git_wrapper::status::types::ChangelistState::{Conflicted, Staged, Unstaged};

impl Render for ChangelistState {
    fn render(&self) -> String {
        match self {
            Staged => "Changes staged to be committed",
            Unstaged => "Unstaged changes that will not be committed",
            Conflicted => "Conflicting changes",
            _ => unimplemented!(),
        }
        .to_owned()
    }
}
