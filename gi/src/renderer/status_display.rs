use serde::ser::SerializeStruct;
use serde::{Serialize, Serializer};

use git_wrapper::status::types::FileState;
use git_wrapper::StatusEntry;
use FileState::*;

use super::render_trait::Render;

// There is a slight muddling of concerns here, insofar as this struct contains
// status data and rendering metadata. The merit of being able to build all this
// information once and then hand it to the template renderer, versus the
// alternative, was deemed worth this bit of impurity

#[derive(Clone)]
pub struct StatusRenderData {
    pub name: String,
    pub path: String,
    pub status: String,
    pub file_state: FileState,
    pub style: Option<String>,
    pub end_style: Option<String>,
}

impl StatusRenderData {
    pub fn add_status_styles(&mut self) {
        let (start, end) = match &self.file_state {
            Modified | New | Deleted => (
                self.file_state.render(),
                format!("end-{}", &self.file_state.render()),
            ),
            _ => (String::from(""), String::from("")),
        };

        self.style = Some(start);
        self.end_style = Some(end);
    }
}

impl From<StatusEntry> for StatusRenderData {
    fn from(entry: StatusEntry) -> Self {
        let path = entry.path.clone();
        let status = entry.status.render();
        let mut path = path.rsplitn(2, '/');

        let name = path.next().unwrap().to_owned();
        let path = path.next().unwrap_or("./").to_owned();

        StatusRenderData {
            name,
            path,
            status,
            file_state: entry.status.file_state,
            style: None,
            end_style: None,
        }
    }
}

impl Serialize for StatusRenderData {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_struct("Status", 3)?;
        state.serialize_field("name", &self.name)?;
        state.serialize_field("path", &self.path)?;
        state.serialize_field("status", &self.status)?;

        state.serialize_field::<String>("file_state", &self.file_state.render())?;

        if let Some(name_style) = &self.style {
            state.serialize_field("name_style", name_style)?;
        };

        if let Some(name_style_end) = &self.end_style {
            state.serialize_field("name_style_end", name_style_end)?;
        }

        state.end()
    }
}
