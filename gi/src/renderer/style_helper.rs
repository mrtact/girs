use handlebars::{Context, Handlebars, Helper, HelperResult, Output, RenderContext, RenderError};

pub fn style_helper(
    h: &Helper<'_, '_>,
    _: &Handlebars<'_>,
    context: &Context,
    _: &mut RenderContext<'_, '_>,
    out: &mut dyn Output,
) -> HelperResult {
    let id = h
        .param(0)
        .expect("'style' helper requires an argument (the style name");

    // The value is automatically looked up at parse time in the
    // local context. Therefore, if it is the name of a variable,
    // it will be resolved to the variable's value. If it's not,
    // we want to use the path specified as the key.
    let id = if id.value().is_null() {
        id.relative_path().map_or("", String::as_str)
    } else {
        id.value().as_str().unwrap_or_default()
    };

    let style = &context.data()["styles"][id]
        .as_str()
        .unwrap_or_default()
        .to_owned();

    out.write(style)
        .map_err(|e| RenderError::from_error("style helper", e))
}
