use anyhow::anyhow;
use std::env::current_exe;
use std::fs::File;

use crossterm::style::ContentStyle;
use path_macro::path;
use serde_json::{Map, Value};

use crate::renderer::style::{AnsiEnd, AnsiStart};

use super::style::Style;

pub fn styles() -> anyhow::Result<Box<Value>> {
    let path = current_exe()?;
    let path = path
        .parent()
        .ok_or_else(|| anyhow!("Could not get path of the girs executable"))?;

    let path = path!(path / "resources/themes/dark.yaml");
    let file = File::open(path)?;
    let mut styles = Map::new();

    let yaml: Value = serde_yaml::from_reader(file)?;
    if let Value::Object(map) = yaml {
        map.iter().for_each(|(key, arr)| {
            if let Value::Array(arr) = arr {
                let mut style = ContentStyle::new();
                arr.iter().for_each(|entry| {
                    let mut entry = entry.as_str().unwrap();
                    let mut bg = false;
                    if entry.starts_with("bg-") {
                        entry = entry.get(3..).unwrap();
                        bg = true;
                    }

                    match Style::from(entry) {
                        Style::Attribute(attr) => style.attributes.set(attr),
                        Style::Color(color) => {
                            if bg {
                                style.background_color = Some(color);
                            } else {
                                style.foreground_color = Some(color);
                            }
                        }
                        Style::None => (),
                    }
                });

                styles.insert(key.to_string(), Value::String(style.start_token()));
                styles.insert(format!("end-{}", key), Value::String(style.end_token()));
            }
        });
    } else {
        panic!("Style file is not an object");
    }

    let styles = Value::Object(styles);

    Ok(Box::new(styles))
}
