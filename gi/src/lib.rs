pub mod global;
pub mod errors;
pub mod executors;
pub mod renderer;
pub mod router;
pub mod util;

pub use fluent::FluentValue;
pub use sugars::hmap;

use std::env;
use std::path::PathBuf;

use itertools::Itertools;
use log::{debug, info};
use path_macro::path;

use gi_parser::{build_lookup_table, parse, GiParserError};

use crate::global::{Loc, US_ENGLISH};

pub fn main() {
    // Set up global dependencies
    env_logger::init();
    Loc::init(US_ENGLISH, get_loc_path());

    // Get the command line arguments as a single string, skipping the exe name
    let cmdline = env::args().skip(1usize).join(" ");

    // Parse the command line arguments
    let result = parse(build_lookup_table(), &cmdline);

    // Execute the command
    match result {
        Ok(mut verb_phrase) => {
            debug!("{:?}", verb_phrase);

            let cmd = router::route_command(&verb_phrase);
            cmd.unwrap().exec(&mut verb_phrase).unwrap();
        }
        Err(e) => {
            handle_error(e);
        }
    };
}

fn get_loc_path() -> PathBuf {
    // let exe_dir = current_exe().unwrap();
    // let exe_dir = exe_dir.parent().unwrap();
    // let loc_path = path!(exe_dir / "resources" / "locales");
    let cwd = env::current_dir().unwrap();
    let loc_path = path!(cwd / "resources" / "locales");

    println!("loc path: {}", loc_path.display());
    info!("loc path: {}", loc_path.display());
    loc_path
}

fn handle_error(err: GiParserError<&str>) {
    println!("Error: {:?}", err);
}
