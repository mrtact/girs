use multimap::MultiMap;
use serde_json::{json, Value};

use gi_parser::parser::VerbPhrase;
use git_wrapper::repo::Repo;

use crate::global::Loc;
use crate::renderer::render_trait::Render;
use crate::renderer::status_display::StatusRenderData;
use crate::renderer::styles::styles;
use crate::renderer::Renderer;
use crate::executors::git_command::GitCommand;

pub struct ListChanges;

impl GitCommand for ListChanges {
    fn exec(&self, _verb_phrase: &mut VerbPhrase) -> anyhow::Result<()> {
        let repo = Repo::new()?;
        list_changes(&repo)
    }

    fn help(&self) -> anyhow::Result<()> {
        println!("List changes command");
        Ok(())
    }

    fn complete(&self, _verb_phrase: &VerbPhrase) -> anyhow::Result<()> {
        todo!()
    }
}

fn list_changes(repo: &Repo) -> anyhow::Result<()> {
    let branch = repo.get_current_branch()?;
    let branch_name = branch.name();

    let remote = branch.upstream();

    let remote_name = if let Some(rb) = remote {
        let rb_name = rb.name();
        let (remote, rb_name) = rb_name
            .split_once('/')
            .expect("No remote name in remote path");

        // If branch and remote branch name are the same, just show the
        // remote name. This helps prevent long branch names from wrapping
        if branch_name == rb_name {
            format!("{}/*", remote)
        } else {
            rb_name.to_owned()
        }
    } else {
        "".to_owned()
    };

    let ab = repo.get_branch_ahead_behind(&branch)?;

    let statuses: MultiMap<String, StatusRenderData> = repo
        .get_statuses()
        .into_iter()
        .map(|entry| {
            let changelist_state = entry.status.changelist_state.clone();
            let mut srd = StatusRenderData::from(entry);
            srd.add_status_styles();

            (changelist_state.render(), srd)
        })
        .collect();
    //     group_statuses_by_type(statuses);
    // add_status_styles(&mut statuses);

    let styles = styles()?;
    let Loc { loader: _, lang_id } = Loc::global();

    let pulled = repo.get_pulled_date()?.render();

    let data: Value = json!({
        "lang": format!("{}", lang_id),
        "branch": branch_name,
        "ab": ab.render(),
        "remote": remote_name,
        "pulled": pulled,
        "statuses": statuses,
        "styles": styles
    });

    // println!("{}", serde_json::to_string_pretty(&data).unwrap());

    let mut renderer = Renderer::new();
    renderer.add_templates("list/changes");
    renderer.render(&data)
}
