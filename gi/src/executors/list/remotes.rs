use crate::executors::git_command::GitCommand;
use gi_parser::parser::VerbPhrase;
use git_wrapper::repo::Repo;

pub struct ListRemotes;

impl GitCommand for ListRemotes {
    fn exec(&self, _verb_phrase: &mut VerbPhrase) -> anyhow::Result<()> {
        let _repo = Repo::new()?;
        todo!()
    }

    fn help(&self) -> anyhow::Result<()> {
        todo!()
    }

    fn complete(&self, _verb_phrase: &VerbPhrase) -> anyhow::Result<()> {
        todo!()
    }
}
