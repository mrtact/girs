use serde::Serialize;
use serde_json::json;

use gi_parser::parser::VerbPhrase;

use crate::renderer::render_trait::Render;
use crate::renderer::styles::styles;
use crate::renderer::Renderer;
use crate::executors::git_command::GitCommand;
use git_wrapper::repo::Repo;

pub struct ListBranches;

#[derive(Serialize)]
struct BranchDescription {
    name: String,
    r#type: String,
}

impl GitCommand for ListBranches {
    fn exec(&self, _verb_phrase: &mut VerbPhrase) -> anyhow::Result<()> {
        let repo = Repo::new()?;
        let branches: Vec<BranchDescription> = repo
            .branches()
            .map(|branch| BranchDescription {
                name: branch.name(),
                r#type: branch.branch_type().render(),
            })
            .collect();

        let styles = styles()?;
        let data = json!({
            "branches": branches,
            "styles": styles
        });

        // TODO
        // let Loc { loader: _, lang_id } = Loc::global();

        let mut renderer = Renderer::new();
        renderer.add_templates("list/branches");
        renderer.render(&data)
    }
}
