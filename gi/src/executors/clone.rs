use std::env::current_dir;
use std::fs::{remove_dir_all, rename};

use anyhow::anyhow;
use log::{debug, error};
use path_macro::path;
use symlink::symlink_dir;

use gi_parser::parser::VerbPhrase;
use gi_parser::util::take_word;
use git_wrapper::repo::Repo;
use git_wrapper::util::cache_dir;
use git_wrapper::util::is_repo;
use git_wrapper::util::repo_url::RepoUrl;

use crate::errors::GiError::{CloneRequiresRemote, DirectoryExists};
use crate::executors::git_command::GitCommand;

pub struct CloneCommand;

impl GitCommand for CloneCommand {
    // TODO: This needs to also handle https & local urls
    fn exec(&self, verb_phrase: &mut VerbPhrase) -> anyhow::Result<()> {
        let args: &String = &verb_phrase.args;

        let (input, remote) = take_word()(args.as_str()).map_err(|_| CloneRequiresRemote)?;

        let local = take_word()(input).ok().map(|(_, x)| x);

        let repo_url = RepoUrl::try_from(remote).map_err(|_| anyhow!("Could not get repo URL"))?;

        // Because we can't figure out what the name of the main branch is without
        // a working local repo, we first clone the main worktree into a directory
        // called "default", which we will rename afterward.
        // TODO: Can we do this with a shallow clone? Or even an orphan-type thing?
        let worktree_path = remote.replace('/', "_");
        let worktree_path = path!(cache_dir::get().as_path() / worktree_path / "default");

        debug!(
            "Cloning main branch into worktree path {}",
            worktree_path.display()
        );

        cache_dir::create_if_not_exists(&worktree_path);

        if is_repo(&worktree_path) {
            debug!(
                "Clone path {} already exists, skipping",
                &worktree_path.display()
            );
        } else {
            debug!("Cloning {} into {}", &remote, &worktree_path.display());
            let _ = Repo::git_clone(remote, &worktree_path).map_err(|err| {
                error!("{:?}", err);
                anyhow!(err)
            })?;
        }

        // Rename the dummy "default" directory to the main branch name.
        let repo = Repo::at(&worktree_path)?;
        let main_name = repo.default_branch_name()?;
        let main_path = path!(worktree_path.parent().unwrap() / main_name);

        // If the main branch already exists, we have already cloned this repo.
        // Simply delete what we just cloned and link the existing main branch
        // directory, so we don't muck up any other checkouts of that branch that
        // may exist on this disk.
        if main_path.exists() {
            remove_dir_all(&worktree_path).unwrap();
        } else {
            rename(&worktree_path, &main_path)?;
        }

        let mut local_path = if let Some(local) = local {
            path!(local / &repo_url.name)
        } else {
            path!(&repo_url.name)
        };

        if local_path.is_relative() {
            local_path = path!(current_dir()? / local_path);
        }

        // TODO: Exists and is not empty... should be valid to clone into empty ./
        if local_path.exists() {
            let path_str = format!("{}", local_path.display());
            return Err(DirectoryExists(path_str).into());
        }

        // Symlink the main branch to the subdirectory
        debug!(
            "Linking from {} into {}",
            main_path.display(),
            local_path.display()
        );

        symlink_dir(&main_path, &local_path)?;

        Ok(())
    }

    fn help(&self) -> anyhow::Result<()> {
        todo!()
    }

    fn complete(&self, _verb_phrase: &VerbPhrase) -> anyhow::Result<()> {
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use path_macro::path;
    use temp_dir::TempDir;

    use gi_parser::parser::VerbClause::IntransitiveVerb;
    use gi_parser::parser::VerbPhrase;
    use gi_parser::tokens::Verb::Clone;
    use speculoos::prelude::*;
    use test_setup::RepoBuilder;

    use crate::executors::clone::CloneCommand;
    use crate::executors::git_command::GitCommand;

    #[test]
    fn can_clone() {
        let setup = RepoBuilder::new()
            .with_test_file("readme.txt", "Test file")
            .build();

        let target_dir = TempDir::new().unwrap();
        let cmd = CloneCommand {};
        let mut phrase = VerbPhrase {
            meta: None,
            clause: Some(IntransitiveVerb(Clone)),
            args: format!(
                "{} {}",
                setup.repo_path.path().display(),
                target_dir.path().display()
            ),
        };
        cmd.exec(&mut phrase).unwrap();

        let readme = path!(target_dir.path() / "test" / "readme.txt");

        assert_that(&readme).matches(|o| o.as_path().exists());
    }
}
