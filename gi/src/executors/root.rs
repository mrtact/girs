use crate::executors::git_command::GitCommand;
use gi_parser::parser::VerbPhrase;

pub struct RootCommand;

impl GitCommand for RootCommand {
    fn exec(&self, _verb_phrase: &mut VerbPhrase) -> anyhow::Result<()> {
        self.help()
    }

    fn help(&self) -> anyhow::Result<()> {
        println!("gi man page");
        Ok(())
    }

    fn complete(&self, _verb_phrase: &VerbPhrase) -> anyhow::Result<()> {
        todo!()
    }
}
