use crate::router;
use crate::executors::git_command::GitCommand;
use gi_parser::parser::VerbPhrase;

pub struct HelpCommand;

impl GitCommand for HelpCommand {
    fn exec(&self, verb_phrase: &mut VerbPhrase) -> anyhow::Result<()> {
        verb_phrase.meta = None;
        let cmd = router::route_command(verb_phrase).unwrap();
        cmd.help()
    }
}
