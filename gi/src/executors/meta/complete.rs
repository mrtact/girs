use crate::executors::git_command::GitCommand;
use gi_parser::parser::VerbPhrase;

pub struct CompleteCommand;

impl GitCommand for CompleteCommand {
    fn exec(&self, _verb_phrase: &mut VerbPhrase) -> anyhow::Result<()> {
        todo!()
    }
}
