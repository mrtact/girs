use gi_parser::parser::VerbPhrase;

pub trait GitCommand {
    fn exec(&self, verb_phrase: &mut VerbPhrase) -> anyhow::Result<()>;

    fn help(&self) -> anyhow::Result<()> {
        unimplemented!();
    }

    fn complete(&self, _verb_phrase: &VerbPhrase) -> anyhow::Result<()> {
        unimplemented!();
    }
}
