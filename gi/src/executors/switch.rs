use std::fs::rename;

use anyhow::bail;
use log::debug;
use path_macro::path;
use sugars::hmap;
use symlink::symlink_dir;

use gi_parser::parser::VerbPhrase;
use gi_parser::util::take_word;
use git_wrapper::repo::Repo;
use git_wrapper::{Branch, Local, Remote};
use GiError::MissingBranchSpecifier;

use crate::errors::GiError;
use crate::errors::GiError::{DirectoryExists, MissingWorktree};
use crate::util::{count_down, root_symlink};
use crate::{loc, loc1};
use crate::executors::git_command::GitCommand;


pub struct SwitchCommand;

/*
   If the cache contains the named branch, switch to it.
   If it doesn't, but there is a remote branch with that name, track it (on countdown)
   If neither, create the branch (on countdown)
*/
impl GitCommand for SwitchCommand {
    fn exec(&self, verb_phrase: &mut VerbPhrase) -> anyhow::Result<()> {
        let repo = Repo::new()?;
        let (_, next_branch) =
            take_word()(&verb_phrase.args).map_err(|_| MissingBranchSpecifier)?;

        if repo.has_worktree(next_branch) {
            // TODO: Assumes the existence of the directory indicates a
            //  valid worktree
            checkout_local_branch(&repo, next_branch)?;
            return Ok(());
        }

        if let Some(branch) = repo.get_branch(next_branch) {
            // Check out an existing branch
            return match branch.branch_type() {
                Local => Ok(()), // TODO: does this indicate some kind of error condition?
                Remote => track_remote(&repo, &branch),
            };
        }

        // No existing branch with the specified name exists, so offer
        // to create one
        count_down(
            &loc!(
                "switch-create",
                hmap!("branch".to_owned() => next_branch.into())
            ),
            &loc!("creating"),
            || {
                println!("New branch created");
                let new = repo.create_branch(&repo.get_current_branch()?, next_branch)?;

                repo.checkout_branch(&new.name())
            },
        )?;

        Ok(())
    }

    fn complete(&self, _verb_phrase: &VerbPhrase) -> anyhow::Result<()> {
        unimplemented!()
    }
}

fn checkout_local_branch(repo: &Repo, worktree_name: &str) -> anyhow::Result<()> {
    let worktree_dir = repo.worktree_dir(worktree_name)?;

    if !worktree_dir.exists() {
        debug!("Worktree dir doesn't exist, cannot link to it... bailing");
        bail!(MissingWorktree(
            worktree_dir.as_path().display().to_string()
        ));
    }

    // Remove the symlink at the root of the repo & recreate it
    // pointing to the new dir
    let root = root_symlink(&repo).ok_or(DirectoryExists(String::from("Bad call, Ripley")))?;

    debug!("Symlink root: {}", root.display());
    debug!("Worktree directory in cache: {}", worktree_dir.display());

    let deleteme = path!(root.parent().unwrap() / "deleteme");
    rename(&root, &deleteme)?;
    symlink_dir(&root, &worktree_dir)?;

    Ok(())

    // Wrapper shellscript will cd us back into the working dir,
    // so we don't end up in an invalid directory state
    // TODO: What happens when you are in a directory that goes away
    //  after the directory switch?
}

// Matching remote exists, but no local tracking branch has been
// created yet
fn track_remote(repo: &Repo, remote_branch: &Branch) -> anyhow::Result<()> {
    let branch_name = remote_branch.name();
    let (_remote, branch_name) = branch_name.split_once('/').unwrap();
    let worktree_dir = repo.worktree_dir(&branch_name)?;

    if worktree_dir.exists() {
        let worktree = worktree_dir.display().to_string();
        debug!("Worktree dir {} already exists", worktree);
        bail!(MissingWorktree(worktree));
    }

    count_down(
        &loc1!("switch-track", branch_name),
        &loc!("tracking"),
        || {
            println!("Remote branch tracked");
            let tracking_branch = repo.create_tracking_branch(remote_branch, branch_name)?;
            repo.create_worktree(tracking_branch, &branch_name)?;
            checkout_local_branch(repo, &branch_name)
        },
    )
}
