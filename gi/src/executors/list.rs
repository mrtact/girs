pub use branches::ListBranches;
pub use changes::ListChanges;
use gi_parser::parser::VerbPhrase;
pub use remotes::ListRemotes;

use crate::executors::git_command::GitCommand;

mod branches;
mod changes;
mod remotes;

pub struct ListCommand;

impl GitCommand for ListCommand {
    fn exec(&self, verb_phrase: &mut VerbPhrase) -> anyhow::Result<()> {
        ListChanges {}.exec(verb_phrase)
    }

    fn help(&self) -> anyhow::Result<()> {
        println!("gi list [branches|changes]");
        Ok(())
    }

    fn complete(&self, _verb_phrase: &VerbPhrase) -> anyhow::Result<()> {
        todo!()
    }
}
