_gi_complete() {
  local word completions
  word="$1"
  completions="$(gi complete "${word}")"
  reply=( "${(ps:\n:)completions}" )
}

compctl -f -K _gi_complete gi
